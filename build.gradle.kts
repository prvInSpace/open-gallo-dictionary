import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.7.0"
    id("jacoco")
    id("maven-publish")
    id("java-library")
    id("de.jansauer.printcoverage") version "2.0.0"
}

group = "cymru.prv"
version = "1.2-SNAPSHOT-1"
val osdVersion = "[1.2.0,1.3.0)"

repositories {
    mavenCentral()
    maven(url="https://gitlab.com/api/v4/projects/24450764/packages/maven")
}

dependencies {
    implementation ("cymru.prv:open-dictionary:${osdVersion}")
    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
    finalizedBy(tasks.jacocoTestReport)
}

tasks.jacocoTestReport {
    reports {
        xml.required.set(true)
        xml.outputLocation.set(file("${project.buildDir}/reports/jacoco/test/jacocoTestReport.xml"))
        html.required.set(true)
    }
    dependsOn(tasks.test)
    finalizedBy(tasks.printCoverage)
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "11"
}

java {
    withJavadocJar()
    withSourcesJar()
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            artifactId = "open-gallo-dictionary"
            from(components["java"])
        }
    }
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/37661215/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}
