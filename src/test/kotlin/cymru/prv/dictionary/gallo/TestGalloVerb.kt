package cymru.prv.dictionary.gallo

import org.json.JSONObject
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

/**
 * @author Preben Vangberg
 * @since 1.2.0
 */
class TestGalloVerb {

    @Test
    fun `should be able to override stem`(){
        val verb = GalloVerb(JSONObject()
            .put("normalForm", "tester")
            .put("stem", "new-stem")
        )
        Assertions.assertEquals("new-stem", verb.stem)
    }

}