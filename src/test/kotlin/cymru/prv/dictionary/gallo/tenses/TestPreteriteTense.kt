package cymru.prv.dictionary.gallo.tenses

import org.junit.jupiter.api.Test

/**
 * @author Preben Vangberg
 * @since 1.2.0
 */
class TestPreteriteTense: TestTense("preterite") {

    @Test
    fun `ER class verbs should export correct forms`() =
        testDefaultTense("cramper",
            "crampis", "crampis", "crampit",
            "crampime", "crampite", "crampirent")

    @Test
    fun `IR class verbs should export correct forms`() =
        testDefaultTense("foncir",
            "foncis", "foncis", "foncit",
            "foncime", "foncite", "foncirent")

}