package cymru.prv.dictionary.gallo.tenses

import org.junit.jupiter.api.Test

/**
 * @author Preben Vangberg
 * @since 1.2.0
 */
class TestPresentTense: TestTense("present") {


    @Test
    fun `ER class verbs should export correct forms`() =
        testDefaultTense("cramper",
            "crampe", "crampe", "crampe",
            "crampom", "crampez", "crampent")

    @Test
    fun `IR class verbs should export correct forms`() =
        testDefaultTense("foncir",
            "foncis", "foncis", "foncit",
            "foncissom", "foncissez", "foncissent")



}