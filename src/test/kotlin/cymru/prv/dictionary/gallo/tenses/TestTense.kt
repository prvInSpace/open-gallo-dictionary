package cymru.prv.dictionary.gallo.tenses

import cymru.prv.dictionary.gallo.GalloVerb
import org.json.JSONObject
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

/**
 * @author Preben Vangberg
 */
abstract class TestTense(val tense: String) {

    fun fetchTense(word: String, obj: JSONObject = JSONObject()): JSONObject {
        val verb = GalloVerb(JSONObject().put("normalForm", word)
            .put(tense, obj)
        )
        return verb.toJson()
            .getJSONObject("conjugations")
            .getJSONObject(tense)
    }

    fun testDefaultTense(word: String,
                  singFirst: String,
                  singSecond: String,
                  singThird: String,
                  plurFirst: String,
                  plurSecond: String,
                  plurThird: String,
    ){
        val obj = fetchTense(word, JSONObject())
        fun JSONObject.gs(key: String): String {
            return this.getJSONArray(key)[0] as String
        }
        Assertions.assertEquals(singFirst, obj.gs("singFirst"))
        Assertions.assertEquals(singSecond, obj.gs("singSecond"))
        Assertions.assertEquals(singThird, obj.gs("singThird"))
        Assertions.assertEquals(plurFirst, obj.gs("plurFirst"))
        Assertions.assertEquals(plurSecond, obj.gs("plurSecond"))
        Assertions.assertEquals(plurThird, obj.gs("plurThird"))
    }

    @Test
    fun `test exports tense`(){
        Assertions.assertDoesNotThrow { fetchTense(tense) }
    }

    @Test
    fun `test all overrides`(){
        val tenses = listOf(
            "singFirst", "singSecond", "singThird",
            "plurFirst", "plurSecond", "plurThird"
        )
        val tense = fetchTense("test", tenses.foldRight(JSONObject()) { tense, obj ->
            obj.put(tense, tense)
        })

        tenses.forEach {
            Assertions.assertEquals(it,
            tense.getJSONArray(it)[0])
        }
    }

}