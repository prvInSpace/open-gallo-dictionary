package cymru.prv.dictionary.gallo.tenses

import cymru.prv.dictionary.common.Conjugation
import cymru.prv.dictionary.gallo.GalloVerb
import org.json.JSONObject

/**
 * @author Preben Vangberg
 */
abstract class GalloVerbTense(obj: JSONObject, verb: GalloVerb): Conjugation(obj) {

    val stem = obj.optString("stem", verb.stem)
    val conjugationClass = obj.optString("class", null)
        ?.let { GalloVerb.ConjugationClass.valueOf(it.uppercase()) }
        ?: verb.conjugationClass

    abstract override fun getDefaultSingularFirst(): MutableList<String>
    abstract override fun getDefaultSingularSecond(): MutableList<String>
    abstract override fun getDefaultSingularThird(): MutableList<String>
    abstract override fun getDefaultPluralFirst(): MutableList<String>
    abstract override fun getDefaultPluralSecond(): MutableList<String>
    abstract override fun getDefaultPluralThird(): MutableList<String>
}