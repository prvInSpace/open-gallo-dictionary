package cymru.prv.dictionary.gallo.tenses

import cymru.prv.dictionary.gallo.GalloVerb
import cymru.prv.dictionary.gallo.GalloVerb.*
import org.json.JSONObject

/**
 * @author Preben Vangberg
 */
class PresentTense(obj: JSONObject, verb: GalloVerb): GalloVerbTense(obj, verb) {

    override fun getDefaultSingularFirst(): MutableList<String> {
        return mutableListOf(when(conjugationClass){
            ConjugationClass.ER -> stem + "e"
            ConjugationClass.IR -> stem + "is"
        })
    }

    override fun getDefaultSingularSecond(): MutableList<String> {
        return mutableListOf(when(conjugationClass){
            ConjugationClass.ER -> stem + "e"
            ConjugationClass.IR -> stem + "is"
        })
    }

    override fun getDefaultSingularThird(): MutableList<String> {
        return mutableListOf(when(conjugationClass){
            ConjugationClass.ER -> stem + "e"
            ConjugationClass.IR -> stem + "it"
        })
    }

    override fun getDefaultPluralFirst(): MutableList<String> {
        return mutableListOf(when(conjugationClass){
            ConjugationClass.ER -> stem + "om"
            ConjugationClass.IR -> stem + "issom"
        })
    }

    override fun getDefaultPluralSecond(): MutableList<String> {
        return mutableListOf(when(conjugationClass){
            ConjugationClass.ER -> stem + "ez"
            ConjugationClass.IR -> stem + "issez"
        })
    }

    override fun getDefaultPluralThird(): MutableList<String> {
        return mutableListOf(when(conjugationClass){
            ConjugationClass.ER -> stem + "ent"
            ConjugationClass.IR -> stem + "issent"
        })
    }
}