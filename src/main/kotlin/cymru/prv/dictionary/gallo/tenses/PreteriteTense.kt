package cymru.prv.dictionary.gallo.tenses

import cymru.prv.dictionary.gallo.GalloVerb
import org.json.JSONObject

/**
 * @author Preben Vangberg
 */
class PreteriteTense(obj: JSONObject, verb: GalloVerb): GalloVerbTense(obj, verb)  {

    override fun getDefaultSingularFirst(): MutableList<String> {
        return mutableListOf(stem + "is")
    }

    override fun getDefaultSingularSecond(): MutableList<String> {
        return mutableListOf(stem + "is")
    }

    override fun getDefaultSingularThird(): MutableList<String> {
        return mutableListOf(stem + "it")
    }

    override fun getDefaultPluralFirst(): MutableList<String> {
        return mutableListOf(stem + "ime")
    }

    override fun getDefaultPluralSecond(): MutableList<String> {
        return mutableListOf(stem + "ite")
    }

    override fun getDefaultPluralThird(): MutableList<String> {
        return mutableListOf(stem + "irent")
    }
}