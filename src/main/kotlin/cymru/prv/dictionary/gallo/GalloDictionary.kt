package cymru.prv.dictionary.gallo

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.common.DictionaryList

/**
 * @author Preben Vangberg
 * @since 1.2.0
 */
class GalloDictionary(list: DictionaryList = DictionaryList()): Dictionary(
    list,
    "gallo",
    mapOf(

    )
) {
}