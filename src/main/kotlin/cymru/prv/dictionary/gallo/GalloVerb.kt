package cymru.prv.dictionary.gallo

import cymru.prv.dictionary.common.Word
import cymru.prv.dictionary.common.WordType
import cymru.prv.dictionary.gallo.tenses.GalloVerbTense
import cymru.prv.dictionary.gallo.tenses.PresentTense
import cymru.prv.dictionary.gallo.tenses.PreteriteTense
import org.json.JSONObject

/**
 * @author Preben Vangberg
 */
class GalloVerb(obj: JSONObject): Word(obj, WordType.verb) {

    enum class ConjugationClass {
        ER, IR
    }

    companion object {

        val availableTenses = mapOf<String, (JSONObject, GalloVerb) -> GalloVerbTense>(
            "present" to { o, w -> PresentTense(o,w) },
            "preterite" to { o, w -> PreteriteTense(o, w) }
        )

    }

    val stem = obj.optString("stem",
        normalForm.replace("(er|ir|re)$".toRegex(), ""))

    val conjugationClass = when {
        obj.has("class") -> ConjugationClass.valueOf(obj.getString("class").uppercase())
        normalForm.endsWith("ir") -> ConjugationClass.IR
        else -> ConjugationClass.ER
    }

    // Creates a map of all of the tense overrides
    val tenses = availableTenses.entries
        .filter { obj.has(it.key) }
        .associate { it.key to it.value.invoke(obj.getJSONObject(it.key), this) }

    override fun getConjugations(): JSONObject? {
        // Goes through and add all the tenses from available
        // tenses. If an override exist in tenses then that is
        // added instead.
        val tensesToExport = availableTenses.keys
            .associateWith { when {
                tenses.containsKey(it) -> tenses[it]!!
                else -> availableTenses[it]!!.invoke(JSONObject(), this)
            }}
            .filter { it.value.has() }
            .toMap()
        if(tensesToExport.isEmpty())
            return null
        return tensesToExport.entries.fold(JSONObject())
            { obj, tense -> obj.put(tense.key, tense.value.toJson())}
    }

}